let texto = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut suscipit condimentum justo eget volutpat. Morbi felis diam, pellentesque sed aliquam eu, aliquam vitae elit. Integer ut sapien est. Nulla at elit nec nisl dignissim viverra sit amet in libero. Nunc sed bibendum enim. Praesent pharetra semper nulla vitae interdum. Nunc interdum enim imperdiet porta bibendum. Aenean ex ipsum, placerat sed sollicitudin et, tempus non sapien. Suspendisse congue vulputate molestie. Cras auctor vehicula justo tincidunt accumsan. Pellentesque vel iaculis nibh. Aliquam scelerisque eleifend vestibulum.Phasellus non dictum eros. Praesent cursus laoreet ipsum, in porta nisi hendrerit eu. Pellentesque scelerisque felis ut nunc sagittis, quis ultricies nunc euismod. Curabitur quis neque in magna efficitur luctus mollis vel odio. In eu condimentum orci. Curabitur ut ex imperdiet, consectetur diam at, vestibulum risus. Nunc pharetra, est eu placerat dapibus, risus odio blandit ex, at aliquam enim augue sit amet lacus. Cras bibendum, quam non ultrices porttitor, leo urna egestas eros, a sagittis ligula erat vitae purus. In sit amet porta turpis.Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae Cras mauris mi, aliquet ac dui non, pellentesque venenatis metus. Integer hendrerit tortor id pharetra ultrices. Suspendisse cursus suscipit congue. Vestibulum ornare faucibus interdum. Aliquam dapibus elit sed lorem laoreet tincidunt. Duis et sem fermentum urna tincidunt rutrum sit amet volutpat elit.';

 function alphabetPosition(text) {
    var chari
    var arr = []
    var alphabet = "1abcdefghijklmnopqrstuvwxyz,." // add 1 para contar o alfabeto a partir a partir de a

    for (var i = 0; i < text.length; i++){
        chari = text[i].toLowerCase() // transforma todas as letras em minuscula 
    
        if (alphabet.indexOf(chari) > -1 && alphabet.indexOf(chari) < 27){
            arr.push(alphabet.indexOf(chari)); // o metodo push inclui na array o índice da ocorrencia das letras contidas em chari na string alphabet
        } else if (alphabet.indexOf(chari) === 27) {
            arr.push(-1)
        } else if (alphabet.indexOf(chari) === 28) {
            arr.push(0)
        }
    }
    return arr.toString(); // transforma array em string 
}
console.log(alphabetPosition(texto));

  